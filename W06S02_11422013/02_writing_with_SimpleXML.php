<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
    $xml_file = 'new_dealer.xml';
    $datas = new simpleXmlElement('<dealer></dealer>');
    $data = $datas->addChild('dealer');
    $data->addChild('kode','0001');
    $data->addChild('nama', 'Toyota 86');
    $data->addChild('stok', '2');

    $priceElement = $data->addChild('harga', '775750000');
    $priceElement->addAttribute('disc', '5/100');
    $data->addChild('kategori','Mobil');

    $data = $datas->addChild('dealer');
    $data->addChild('kode','0002');
    $data->addChild('nama','New Vlus');
    $data->addChild('stok','3');
    $priceElement = $data->addChild('harga','324950000');
    $priceElement->addAttribute('disc','7/100');
    $data->addChild('kategori','Mobil');

    echo('<pre>');
    var_dump($datas);
    $datas->asXML($xml_file);

?>
<!-- Penjelasan
pada kode program diatas adalah berasal dari file new_dealer.xml,Fungsi addChild() menambahkan elemen anak ke elemen SimpleXML.jadi syntax ini ditambahkan ke new_dealer.xml lalu pada kode program diatas dipakai fungsi var_dump() agar di print dengan menggunakan array.
-->