<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<!DOCTYPE html>
<html>
    <head>
        <script>
            var xmlhttp;
            function loadXMLDoc(url,cfunc)
            {
            if(window.XMLHTTPRequest)
            {
                xmlhttp = new XMLHttpRequest();

            } else{
                xmlhttp = new ActivityXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = cfunc;
            xmlhttp.open("GET",url,true);
            xmlhttp.send();
            }
            function myFunction()
            {
                loadXMLDoc("ajax_info.txt", function()
                {
                    if(xmlhttp.readyState === 4 && xmlhttp.status === 200)
                    {
                        document.getElementById("myDiv").innerHTML = xmlhttp.responseText;
                    }
                });
            }
        </script>
    </head>
    <body>
        <div id="myDiv">
            <h2>Let AJAX change this text</h2>
        </div>
        <button type="button" onclick="myFunction()">ChangeContent</button>
    </body>
</html>

<!-- Penjelasan
Pada Kodeprogram diatas adalah ajax dengan menggunakan parameter yang dimana dalam penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request,get adalah metode request yang digunakan dan url adalah alamat url tujuan dan true adalah untuk mengeksekusi AJAX secara asynchronous.Pada kode program diatas adalah untuk mengubah judul.
-->