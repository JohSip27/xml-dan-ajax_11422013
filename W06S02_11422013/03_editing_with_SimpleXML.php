<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
    $xml_file = 'dealer.xml';
    $dealerKitaAja = new simpleXmlElement(file_get_contents($xml_file));
    echo('<pre>');
    var_dump($dealerKitaAja);
    echo($dealerKitaAja -> dealer[1]->kode.'<br>');
    $dealerKitaAja->dealer[1]->kode = 'new2'.$dealerKitaAja->dealer[1]->kode;
    echo($dealerKitaAja->dealer[1]->kode.'<br>');
    echo($dealerKitaAja->dealer[1]->harga->attributes()->disc.'<br>');
    $dealerKitaAja->dealer[1]->harga->attributes()->disc = 7/100;
    echo($dealerKitaAja->dealer[1]->kode.'<br>');
    $new_xml_file = 'new2_'.$xml_file;
    $dealerKitaAja -> asXML($new_xml_file);

?>

<!-- Penjelasan
Pada kode program diatas adalah digunakan untuk untuk mengedit xml yang telah di buat sebelumnya, di edit berdasarkan index array.

=====Pertanyaan=====
Setelah Anda ketik dan jalankan, amatilah kode program dan jelaskan dalam bentuk
komen pemahaman Anda mengenail simple XML. Fungsi apa yang digunakan untuk
menampilkan file XLM, fungsi apa yang digunakan untuk membaca dan mengubah isi dari
file XML?
=====Jawaban=====
Fungsi untuk menampilkan file XML adalah new simpleXmlElement(file_get_contents()) , jika pada kode program diatas maka syntaxnya adalah 
$xml_file = 'dealer.xml';
$dealerKitaAja = new simpleXmlElement(file_get_contents($xml_file));

Fungsi untuk membaca file xml adalah pada file_get_content yang dimana ini merupakan fungsi untuk membaca file xml nya.

fungsi untuk mengubah isi dari file xml dengan cara $dealerKitaAja->dealer[1]->harga->attributes()->disc.'<br>' yang dimana ini nantinya akan diubah file xml nya berdasarkan index array.
-->