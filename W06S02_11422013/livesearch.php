<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
    $xmlDoc = new DOMDocument();
    $xmlDoc -> load("links.xml");
    $x = $xmlDoc -> getElementByTagName("link");
    $q = $_GET["q"];
    if(strlen($q)>0) {
        $hint ="";
        for($i = 0; $i < ($x -> length); $i++) {
            $y = $x -> item($i) -> getElementByTagName('title');
            $z = $x -> item($i) -> getElementByTagName('url');
            if($y -> item(0) ->nodeType == 1) {
                if(stristr($y -> item(0) -> childNodes -> item(0) ->nodeValue, $q)) {
                    if($hint = "") {
                        $hint = "<a href='".$z -> item(0) -> childNodes -> item(0) -> nodeValue ."'target ='_blank'>".$y ->item(0) -> childNodes ->item(0) -> nodeValue. "</a>";
                    } else {
                        $hint = $hint."<br/><a href='".$z -> item(0) -> childNodes -> item(0) -> nodeValue."'target ='_blank'>".$y ->item(0) -> childNodes ->item(0) -> nodeValue. "</a>";

                    }
                }
            }
        }
    }
    if($hint == ""){
        $response = "no suggestion";

    } else {
        $response = $hint;
    }
    echo $response;

?>

<!-- Penjelasan
Pada kode program diatas adalah untuk live search yang datanya diambil dari links.xml, Pada kode program diatas menggunakan looping for untuk menampilkan datanya.
-->