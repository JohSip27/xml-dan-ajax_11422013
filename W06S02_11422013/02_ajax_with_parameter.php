<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<!DOCTYPE html>
    <html>
        <head>
            <script>
                function loadXMLDoc(url)
                {
                    var xmlhttp;
                    if (window.XMLHttpRequest)
                    {
                        xmlhttp = new XMLHttpRequest();
                    }
                    else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");

                    }
                    xmlhttp.onreadystatechange = function()
                    {
                        if(xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                            document.getElementById('p1').innerHTML = "Lasr modified:" + xmlhttp.getResponseHeader(' Last-Modified');
                        }
                    };
                    xmlhttp.open("GET",url,true);
                    xmlhttp.send();
                }
            </script>
        </head>
        <body>
            <p id="p1">The getResponseHeader() function is used to return specific header information from a resource , like length, server-tyoe,content-type,last-modified,etc.</p>
            <button onclick="loadXMLDoc('ajax_info.txt')">Get "Last Modified" information</button>
        </body>
    </html>

    <!-- Penjelasan
Pada Kodeprogram diatas adalah ajax dengan menggunakan parameter yang dimana dalam penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request, get adalah metode request yang digunakan dan url adalah alamat url tujuan dan true adalah untuk mengeksekusi AJAX secara asynchronous, perbedaan kode program ini dengan kode program 01_ajax_without_parameter.php adalah pada kodingan html nya saja, jika pada ajax nya masih sama dan menerapkan 4 langkah tersebut.
-->