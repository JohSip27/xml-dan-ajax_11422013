
<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<!DOCTYPE html>
<html>
    <head>
        <script>
            var xmlhttp;
            function loadXMLDoc(url)
            {
                var xmlhttp;
            if(window.XMLHttpRequest)
            {
                xmlhttp = new XMLHttpRequest();

            } else
            {
                xmlhttp = new ActivityXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function();
            {
                    if(xmlhttp.readyState === 4 && xmlhttp.status === 200)
                    
                    {
                        document.getElementById("A1").innerHTML = xmlhttp.status;
                        document.getElementById("A2").innerHTML = xmlhttp.statusText;
                        document.getElementById("A3").innerHTML = xmlhttp.responseText;

                    }
                };
                xmlhttp.open("GET",url,true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <div id="myDiv">
            <h2>Let AJAX change this text</h2>
        </div>
        <button type="button" onclick="myFunction()">ChangeContent</button>
    </body>
</html>

<!-- Penjelasan
Pada Kodeprogram diatas adalah ajax dengan menggunakan parameter yang dimana dalam penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request, get adalah metode request yang digunakan dan url adalah alamat url tujuan dan true adalah untuk mengeksekusi AJAX secara asynchronous.Perbedaan ajax ini dengan ajax-ajax sebelumnya adalah pada function di ambil id A1,A2,A3(salah satu) yang masing-masing mempunyai tindakan yang berbeda-beda.

=======Dari kode program di atas, dapat kita lihat bahwa ada kaitan antara xml dengan ajax.Jelaskan pemahaman Anda ketika menggunakan xml pada ajax untuk kasus di atas.=====
XML pada kode program diatas fungsi dari xml adalah yang dimana XML ini berguna untuk menyimpan data di file book.xml sehingga pada button onclick="loadXMLDoc('book.xml') sehingga ketika button di click maka fungsi ini akan jalan.
AJAX pada kasus diatas adalah untuk merequest yang dimana penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request"
-->