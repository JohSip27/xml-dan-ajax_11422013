<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<!DOCTYPE html>
<html>
    <head>
        <script>
            function showResult(str) {
                if(str.length == 0) {
                    document.getElementById("livesearch").innerHTML = "";
                    document.getElementById("livesearch").style.border = "0px";
                    return;
                }
                if(window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                    
                } else {
                    xmlhttp = new ActivityXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.open("GET", "livesearch.php?q="+str,true);
                xmlhttp.send();
            }
        </script>
    </head>
    <body>
        <form>
            <input type="text" size ="30" onkeyup = "showResult(this.value)">
            <div id="livesearch"></div>
        </form>
    </body>
</html>

<!-- Penjelasan
Pada Kodeprogram diatas adalah ajax dengan menggunakan parameter yang dimana dalam penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request, pada kode program diatas url nya yaitu livesearch.php?q=.get adalah metode request yang digunakan dan url adalah alamat url tujuan dan true adalah untuk mengeksekusi AJAX secara asynchronous.Pada kode program diatas adalah untuk fitur search dengan menggunakan AJAX.
-->