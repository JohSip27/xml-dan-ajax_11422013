<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php
    $file_name = 'new3_dealer.xml';
    $doc = new DomDocument("1,0");
    $doc -> formatOutput = true;
    $dealerKitaAja = $doc -> createElement('dealerKitaAja');
    $dealerKitaAja = $doc -> appendChild($dealerKitaAja);
    $dealer = $doc -> createElement('dealer');
    $dealer = $dealerKitaAja -> appendChild($dealer);
    $kode = $doc ->createElement('kode');
    $kode  = $dealer -> createChild($kode);
    $kodeValue = $doc -> createTextNode('0001');
    $kode = $dealer -> appendChild($kodeValue);
    $nama = $doc -> createElement('nama');
    $nama = $dealer -> appendChild($nama);
    $namaValue = $doc -> createTextNode('Toyota 86');
    $nama = $nama -> appendChild($namaValue);

    $stok = $doc -> createElement('stok');
    $stok = $dealer -> appendChild($stok);
    $stokValue = $doc -> createTextNode('150');
    $stok = $stok -> appendChild($stokValue);

    $harga = $doc -> createElement('harga');
    $harga = $dealer -> appendChild($harga);
    $harga -> setAttribute('disc','5/100');
    $hargaValue = $doc -> createTextNode('775750000');
    $harga = $harga -> appendChild($hargaValue);

    $kategori = $doc -> createElement('kategori');
    $kategori = $dealer -> appendChild($kategori);
    $kategoriValue = $doc -> createTextNode('Mobil');
    $kategori = $kategori -> appendChild($kategoriValue);
    $doc ->save($file_name);
?>

<!-- Penjelasan
Pada kode program diatas adalah menuliskan xml dengan DOMXML yang dimana ini akan dituliskan pada file xml yang bernama new3_dealer.xml 

=====Pertanyaan=====
Seperti apa cara kerja ajax dengan callback function? Apa yang Anda pahami mengenai callbak
function? Jelaskan.
=====Jawaban=====
Fungsi panggilan balik dipanggil setelah menyelesaikan permintaan AJAX. dan menentukan apa yang harus dilakukan dengan respons yang berasal dari server dalam fungsi callback sehingga cara kerja callback function adalah dengan ketika ada permintaan dari AJAX maka callback function akan datang dan menyelesaikan permintaan AJAX tersebut.
Yang saya pahami dengan fungsi callback adalah Callback pada dasarnya adalah sebuah function. Callback hanya sebuah istilah untuk function yang di passing ke dalam function lain sebagai argument, yang kemudian di eksekusi oleh function yang membungkus function callback tersebut.Dengan kata lain function callback akan di berlakukan sebagai value di dalam function lain sehingga function callback akan di ekseskusi setelah function yang membungkus function callback tersebut selesai di eksekusi. 

-->