<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<?php 
    $file_name= 'dealer.xml';
    $xmlDoc = new DOMDocument();
    $xmlDoc->load($file_name);
    $dealer = $xmlDoc -> getElementsByTagName('dealer');
    foreach($dealer as $dealer) {
        $kode = $dealer -> getElementsByTagName('kode');
        $nama = $dealer -> getElementsByTagName('nama');
        $stok = $dealer -> getElementsByTagName('stok');
        $harga = $dealer -> getElementsByTagName('harga');
        $kategori = $dealer -> getElementsByTagName('kategori');

        echo ('kode:'.$kode -> item(0) ->nodeValue .'<br>');
        echo ('kode:'.$nama -> item(0) ->nodeValue .'<br>');
        echo ('kode:'.$stok -> item(0) ->nodeValue .'<br>');
        echo ('kode:'.$harga -> item(0) ->nodeValue .'(with' .$harga -> item(0) ->getAttribute('disc').'%off)<br>');
        echo ('kategori:'.$kategori -> item(0) ->nodeValue .'<br>');
        echo ('<br>');


    }
?>

<!-- Penjelasan
Pada Kode Program diatas adalah mengedit data dengan DOMXML.dalam penggunaan getElementByTagName nya menggunakan foreach agar terus mengalami perulangan yang dimana datanya diambil dari dealer.xml lalu diubah dengan xmlDoc agar untuk mengubah datanya.

=====Pertanyaan=====
Perhatikan kode program SimpleXML dan DOM XML. Jelaskan perbedaan penerapan XML
menggunakan DOM dengan yang tanpa DOM.
=====Jawaban=====
DOM memodelkan XML sebagai sekumpulan objek node. Node dapat diakses dengan JavaScript atau bahasa pemrograman lainnya.dan perbedaannya ada di syntax pada DOM :
$file_name= 'dealer.xml';
    $xmlDoc = new DOMDocument();
    $xmlDoc->load($file_name);
Pada syntax diatas adalah untuk mengakses file xml nya dengan fungsi new Document()
dan pada syntax pada xml
xmlhttp = new XMLHttpRequest();
xmlhttp = new ActivityXObject("Microsoft.XMLHTTP");
Pada syntax diatas adalah untuk meminta request untuk mengakses xml nya yang dimana penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request. 
-->