<!--
Nama : Johannes Bastian Jasa Sipayung
NIM : 013
Kelas : 41TRPL1
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <script>
        function loadXMLDoc()
        {
            var xmlhttp;
            if(window.XMLHttpRequest)
            xmlhttp = new XMLHttpRequest();
        } else {
            xml = new ActiveXObjek("Microsoft.XMLHTTP");

        }
        xmlhttp.onreadystacthange = function();
        {
            if(xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                document.getElemenById("myDiv"),innerHTML = xmlhttp.responseText;
            }
        };
        xmlhttp.open("GET","ajax_info_txt",true);
        xmlhttp.send();
    
    </script>
</head>
<body>
    <div id="myDiv"><h2> Let AJAX change this text</h2></div>
    <button type="button" onclick="loadXMLDoc()">ChangeContent</button>
</body>
</html>

<!-- Penjelasan
Pada Kodeprogram diatas adalah ajax dengan menggunakan parameter yang dimana dalam penggunaan ajax terdiri dari 4  langkah yaitu dengan pertama kali Membuat Objek Ajax dan yang kedua adalah Menentukan Fungsi Handler untuk Event lalu yang ketiga adalah menentukan Method dan URL dan yang terakhir adalah dengan mengirimkan request, pada kode program diatas url nya yaitu ajax_info_txt.get adalah metode request yang digunakan dan url adalah alamat url tujuan dan true adalah untuk mengeksekusi AJAX secara asynchronous
-->